﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Firefox;

namespace BT_Robot
{
    class Program
    {
        
        static void Main(string[] args)
        {
            IWebDriver driver = new ChromeDriver(@"C:\Users\Einstein\Desktop"); //只support chrome 85  目前chrome version chrome85
            //var driverService = FirefoxDriverService.CreateDefaultService(@"C:\Users\Einstein\Desktop", "geckodriver.exe");
            //driverService.FirefoxBinaryPath = @"C:\Program Files\Mozilla Firefox\firefox.exe";


            //var driver = new FirefoxDriver(driverService, new FirefoxOptions(), TimeSpan.FromSeconds(60));
            //Thread.Sleep(300);

            string URL = "https://webap1.kshs.kh.edu.tw/kshsSSO/publicWebAP/bodyTemp/index.aspx";
            //string[] idnum = {"S125534389", "D123151791", "E225631311", "S125559153", "S125540190", "S125540270", "R125028303", "E125994731", "E126153047" }; //我,林昱安,宋韋柔,葉晉維,陳威瑜,王千才,林彥丞,林子翔,林于翔
            Dictionary<string, string> idinfo = new Dictionary<string, string>();
            idinfo.Add("黃茂勛","S125534389");
            idinfo.Add("林昱安","D123151791");
            idinfo.Add("宋韋柔","E225631311");
            idinfo.Add("葉晉維","S125559153");
            idinfo.Add("陳威瑜","S125540190");
            idinfo.Add("王千才","S125540270");
            idinfo.Add("林彥丞","R125028303");
            idinfo.Add("林子翔","E125994731");
            idinfo.Add("林于翔","E126153047");
            idinfo.Add("蔡佳佑","E125978568");

            //for (int i = 0; i < idinfo.Count; i++)
            //    Console.WriteLine(idinfo.ElementAt(i).Key+" "+idinfo.ElementAt(i).Value);
            //Console.ReadKey();

            
            for (int i=0; i<idinfo.Count; i++)
            {
                driver.Navigate().GoToUrl(URL);
                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10);
                
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                var input_account = driver.FindElement(By.XPath("//*[@id='ContentPlaceHolder1_txtId']"));
                
                input_account.SendKeys(idinfo.ElementAt(i).Value); //陣列多筆

                var login = driver.FindElement(By.XPath("//*[@id='ContentPlaceHolder1_btnId']"));
                login.Click();

                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);

                try //如果有alert框(已填過)
                {
                    var alert = driver.SwitchTo().Alert();
                    alert.Accept();
                    Console.WriteLine("已填 "+ (i+1));
                    continue;
                }
                catch
                { }
             
                var radio = driver.FindElement(By.XPath("//*[@id='ContentPlaceHolder1_rbType_1']"));
                radio.Click();

                var bar1 = driver.FindElement(By.XPath("//*[@id='ContentPlaceHolder1_ddl1']"));
                bar1.SendKeys("36");

                var bar2 = driver.FindElement(By.XPath("//*[@id='ContentPlaceHolder1_ddl2']"));
                bar2.SendKeys("5");

                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);

                var condition = driver.FindElement(By.XPath("//*[@id='ContentPlaceHolder1_ddl3']"));
                //if (idinfo.ElementAt(i).Key.Equals("王千才") || idinfo.ElementAt(i).Key.Equals("林昱安") || idinfo.ElementAt(i).Key.Equals("林子翔"))  //學測前有請假的
                //{
                //    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);
                //    condition.SendKeys("請假→非病假");
                //    Console.Write("請假 ");
                //}
                //else //有來學校
                //{
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);
                    condition.SendKeys("正常");
                //}
                
                var OK = driver.FindElement(By.XPath("//*[@id='ContentPlaceHolder1_btnId0']"));
                OK.Click();

                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);
                try //如果有alert框 (填完確認鍵)
                {
                    var alert = driver.SwitchTo().Alert();
                    alert.Accept();
                }
                catch
                { }
                
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);
                Console.WriteLine("OK "+(i+1)+" th");
                
            }
        }
    }
}
